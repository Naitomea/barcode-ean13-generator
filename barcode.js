// Encodage
var leftEven = [
    "0001101", // 0
    "0011001", // 1
    "0010011", // 2
    "0111101", // 3
    "0100011", // 4
    "0110001", // 5
    "0101111", // 6
    "0111011", // 7
    "0110111", // 8
    "0001011", // 9
];

var leftOdd = [
    "0100111", // 0
    "0110011", // 1
    "0011011", // 2
    "0100001", // 3
    "0011101", // 4
    "0111001", // 5
    "0000101", // 6
    "0010001", // 7
    "0001001", // 8
    "0010111", // 9
];

var leftParts = [leftEven, leftOdd];

var rightPart = [
    "1110010", // 0
    "1100110", // 1
    "1101100", // 2
    "1000010", // 3
    "1011100", // 4
    "1001110", // 5
    "1010000", // 6
    "1000100", // 7
    "1001000", // 8
    "1110100", // 9
];

var codeTable = [
    "000000", // 0
    "001011", // 1
    "001101", // 2
    "001110", // 3
    "010011", // 4
    "011001", // 5
    "011100", // 6
    "010101", // 7
    "010110", // 8
    "011010", // 9
];

// Draws
var scale = 1.5;
var width = 95;
var height = 50;
var startMarge = 10 * scale;

var canvas = document.querySelector("canvas");
var ctx = canvas.getContext("2d");
canvas.width = width * scale + startMarge;

ctx.fillStyle = 'black';
ctx.strokeStyle = 'black';
ctx.lineWidth = 1;
ctx.shadowBlur = 0;

var currentCode = "3068320124537";
var selectedIndex = Number.NaN;

drawBarCode("3068320124537");

function drawBarCode(code)
{
    var drawStartMarge = () => drawMarge(0);
    var drawEndMarge = () => drawMarge(width * scale - 3 * scale);

    // clean
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    // draw
    drawStartMarge();
    drawCentralMarge();
    drawEndMarge();

    ctx.textBaseline = "top";
    ctx.textAlign = "center";
    ctx.font = 9 * scale + "px sans-serif";

    // Barcode
    for (let i = 1; i < code.length; i++) {
        drawBinary(i - 1, code[i], i < 7, code[0]);
    }

    // First Number
    ctx.textAlign = "center";

    if (selectedIndex == -1) {
        ctx.fillRect(0, height + 1, 7 * scale, 9 * scale)
        ctx.fillStyle = "white";
    }

    ctx.fillText(code[0], 3.5 * scale, height + 2, 7);

    if (selectedIndex == -1)
        ctx.fillStyle = "black";
}

function drawBinary(index, value, leftPart = false, leftCode = 0)
{
    let table = codeTable[leftCode];
    let binary = leftPart ? leftParts[table[index]][value] : rightPart[value];
    let startX = (index * 7) * scale;
    
    startX += 3 * scale;
    if (!leftPart)
        startX += 5 * scale;

    // console.log(value, "=>", binary);
    
    // Bars
    for (let i = 0; i < 7; i++) {
        if (binary[i] == "1")
            ctx.fillRect(startMarge + startX + i * scale, 0, 1 * scale, height);
    }

    // Text
    if (index == selectedIndex) {
        ctx.fillRect(startMarge + startX, height + 1, 7 * scale, 9 * scale)
        ctx.fillStyle = "white";
    }

    ctx.fillText(value, startMarge + startX + 3.5 * scale, height + 2, 7);

    if (index == selectedIndex)
        ctx.fillStyle = "black";
}

function drawMarge(startX)
{
    for (let i = 0; i < 3; i++) {
        if (i % 2 != 1)
            ctx.fillRect(startMarge + startX + i * scale, 0, 1 * scale, height + 10);
    }
}

function drawCentralMarge()
{
    let startX = (6 * 7 + 3) * scale;
    for (let i = 0; i < 5; i++) {
        if (i % 2 != 0)
            ctx.fillRect(startMarge + startX + i * scale, 0, 1 * scale, height + 10);
    }
}

// User Interactions
function validCode(val)
{
    val = val.padEnd(13, "0");

    let check = 0;
    for (let i = 0; i < 12; i++) {
        check += val[i] * (i % 2 == 0 ? 1 : 3);
    }
    val = val.replaceAt(12, check % 10 == 0 ? 0 : 10 - check % 10);

    return val;
}

canvas.onclick = function(e) {
    let startX = startMarge + 1.5 * scale;
    let centerX = startMarge + (6 * 7 + 3 + 2.5) * scale;
    let endX = startMarge + width * scale - 1.5 * scale;

    if (e.offsetX < startX)
        selectedIndex = -1;
    else if (e.offsetX < centerX) {
        startX += 1.5 * scale;
        centerX -= 2.5 * scale;
        selectedIndex = Math.max(0, Math.min(5, Math.floor((e.offsetX - startX) / ((centerX - startX) / 6))));
    }
    else {
        centerX += 2.5 * scale;
        endX -= 1.5 * scale;
        selectedIndex = 6 + Math.max(0, Math.min(4, Math.floor((e.offsetX - centerX) / ((endX - centerX) / 6))));
    }

    drawBarCode(currentCode);
}

canvas.onwheel = (e) => {
    if (e.defaultPrevented || Number.isNaN(selectedIndex))
        return;

    let d = e.deltaY > 0 ? 1 : -1;
    selectedIndex -= d;
    selectedIndex = Math.max(-1, Math.min(10, selectedIndex));
    drawBarCode(currentCode);

    e.preventDefault();
}

document.onkeydown = function(e) {
    // console.log(e.key);
    if (e.defaultPrevented)
        return;

    let oldIndex = selectedIndex;
    switch (e.key) {
        case "ArrowLeft":
            selectedIndex -= 1;
            break;
        case "ArrowRight":
        case "Enter":
        case " ":
            if (Number.isNaN(selectedIndex) && e.key == "Enter")
                selectedIndex = 0;
            else
                selectedIndex += 1;
            break;
        case "Backspace":
            if (!Number.isNaN(selectedIndex)) {
                currentCode = validCode(currentCode.replaceAt(selectedIndex + 1, "0"));
                selectedIndex -= 1;
            }
            break;
        case "Delete":
            if (!Number.isNaN(selectedIndex)) {
                currentCode = validCode(currentCode.replaceAt(selectedIndex + 1, "0"));
                drawBarCode(currentCode);
            }
            break;
        case "Escape":
            selectedIndex = Number.NaN;
            break;
        default:
            if (!Number.isNaN(selectedIndex) 
                && Number.isInteger(Number.parseInt(e.key))) {
                    currentCode = validCode(currentCode.replaceAt(selectedIndex + 1, e.key));
                    selectedIndex += 1;
                }
            break;
    }
    
    if (oldIndex != selectedIndex) {
        if (!Number.isNaN(selectedIndex))
            selectedIndex = Math.max(-1, Math.min(10, selectedIndex));
        drawBarCode(currentCode);
    }

    e.preventDefault();
}

// Utils
String.prototype.replaceAt = function(index, replacement) {
    return this.substring(0, index) + replacement + this.substring(index + 1);
}